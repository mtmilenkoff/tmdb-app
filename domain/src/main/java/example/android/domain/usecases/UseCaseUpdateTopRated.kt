package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCaseUpdateTopRated @Inject constructor(private val repo: Repository) {
    suspend operator fun invoke(page: Int){
        repo.updateTopRated(page)
    }
}