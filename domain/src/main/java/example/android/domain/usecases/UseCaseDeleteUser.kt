package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCaseDeleteUser @Inject constructor(private val repo: Repository) {
    suspend operator fun invoke(sessionId: String){
        repo.deleteUser(sessionId)
    }
}