package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCaseObserveSeason @Inject constructor(private val repo: Repository) {
    operator fun invoke(id: Int) = repo.getSeason(id)
}