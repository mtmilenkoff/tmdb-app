package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCaseObserveUser @Inject constructor(private val repo: Repository) {
    operator fun invoke() = repo.observeUser()
}