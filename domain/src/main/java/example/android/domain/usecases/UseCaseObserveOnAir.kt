package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCaseObserveOnAir @Inject constructor(private val repo: Repository) {
    operator fun invoke(page: Int) = repo.getOnAirAndShows(page)
}