package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCaseFavAndShow @Inject constructor(private val repo: Repository) {
    operator fun invoke(page: Int) = repo.getFavAndShow(page)
}