package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCaseUpdateSeason @Inject constructor(private val repo: Repository) {
    suspend operator fun invoke(id: Int){
        repo.updateSeason(id)
    }
}