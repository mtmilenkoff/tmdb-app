package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCaseObserveAiringShows @Inject constructor(private val repo: Repository) {
    operator fun invoke(page: Int) = repo.getAiringAndShows(page)
}