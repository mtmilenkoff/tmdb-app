package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCaseUpdateOnAir @Inject constructor(private val repo: Repository) {
    suspend operator fun invoke(page: Int) {
        repo.updateOnAir(page)
    }
}