package example.android.domain.usecases

import example.android.domain.Repository
import javax.inject.Inject

class UseCasePostLogin @Inject constructor(private val repo: Repository) {
    suspend operator fun invoke(username: String, password: String) = repo.postLogin(username, password)
}