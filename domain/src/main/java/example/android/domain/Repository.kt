package example.android.domain

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import example.android.data.database.DatabaseDao
import example.android.data.model.RequestToken
import example.android.data.model.SessionToken
import example.android.data.model.entity.airingtoday.AiringPageEntity
import example.android.data.model.entity.favorite.FavoritePageEntity
import example.android.data.model.entity.onair.OnAirPageEntity
import example.android.data.model.entity.popular.PopularPageEntity
import example.android.data.model.entity.toprated.TopRatedPageEntity
import example.android.data.model.models.SeasonModel
import example.android.data.model.models.TvShowModel
import example.android.data.network.ApiService
import example.android.data.toEntity
import example.android.data.toModel
import example.android.data.toTvShowModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.await
import javax.inject.Inject

class Repository @Inject constructor(private val apiService: ApiService, private val databaseDao: DatabaseDao) {


    //CATEGORIES
    suspend fun updateTopRated(page: Int){
        withContext(Dispatchers.IO){
            val apiCall = apiService.getTopRatedTvShows(page)
            try{
                val result = apiCall.await()
                result.results.forEach { databaseDao.insertTopRated(TopRatedPageEntity(it.id, page)) }
                databaseDao.insertTvShowList(result.results)
            }catch(e:Error){
                Log.e("REPOSITORY","error on network when calling for Top Rated $e")
            }
        }
    }

    suspend fun updateAiring(page: Int){
        withContext(Dispatchers.IO){
            val apiCall = apiService.getAiringTvShows(page)
            try{
                val result = apiCall.await()
                result.results.forEach { databaseDao.insertAiring(AiringPageEntity(it.id, page)) }
                databaseDao.insertTvShowList(result.results)
            }catch(e:Error){
                Log.e("REPOSITORY","error on network when calling for Top Rated $e")
            }
        }
    }

    suspend fun updatePopular(page: Int){
        withContext(Dispatchers.IO){
            val apiCall = apiService.getPopularTvShows(page)
            try{
                val result = apiCall.await()
                result.results.forEach { databaseDao.insertPopular(PopularPageEntity(it.id, page)) }
                databaseDao.insertTvShowList(result.results)
            }catch(e:Error){
                Log.e("REPOSITORY","error on network when calling for Top Rated $e")
            }
        }
    }

    suspend fun updateOnAir(page: Int){
        withContext(Dispatchers.IO){
            val apiCall = apiService.getOnAirTvShows(page)
            try{
                val result = apiCall.await()
                result.results.forEach { databaseDao.insertOnAir(OnAirPageEntity(it.id, page)) }
                databaseDao.insertTvShowList(result.results)
            }catch(e:Error){
                Log.e("REPOSITORY","error on network when calling for Top Rated $e")
            }
        }
    }

    fun getTopAndShows(page: Int) =
        Transformations.map(databaseDao.observeTopAndShow(page)) {
            it.map { e -> e.toTvShowModel() }
        }

    fun getAiringAndShows(page: Int) =
        Transformations.map(databaseDao.observeAiringAndShow(page)) {
            it.map { e -> e.toTvShowModel() }
        }

    fun getOnAirAndShows(page: Int) =
        Transformations.map(databaseDao.observeOnAirAndShow(page)) {
            it.map { e -> e.toTvShowModel() }
        }

    fun getPopularAndShows(page: Int) =
        Transformations.map(databaseDao.observePopularAndShow(page)) {
            it.map { e -> e.toTvShowModel() }
        }

    //SEASONS
    suspend fun updateSeason(id: Int){
        withContext(Dispatchers.IO){
            val apiCall = apiService.getTvShow(id)
            try{
                val result = apiCall.await()
                databaseDao.insertSeasonPage(result.seasons.map { it.toEntity(id) })
            }catch(e:Error){
                Log.e("REPOSITORY","error on network when calling for Seasons $e")
            }
        }
    }

    fun getSeason(id: Int) =
        Transformations.map(databaseDao.observeSeasons(id)) {
            it.map { e -> e.toModel() }
        }



    suspend fun updateEpisodes(tvId: Int, seasonNumber: Int){
        withContext(Dispatchers.IO){
            val apiCall = apiService.getSeason(tvId, seasonNumber)
            try {
                val result = apiCall.await()
                databaseDao.insertEpisodeList(result.episodes)
            }catch(e:Error){
                Log.e("REPOSITORY","error on network when calling for Episodes $e")
            }
        }
    }

    //USER LOGIN
    suspend fun postLogin(user: String, password: String): Boolean {

        val token = getRequestToken()
        if(token != null){

            val validToken = validateToken(user, password, token)
            if(validToken != null){

                val session = getSession(validToken)
                if(session != null){

                    getUser(user, session.sessionId)
                    getAllFavorites(user, session.sessionId)
                    return session.success
                }
            }
        }
        return false
    }

    private suspend fun getRequestToken(): RequestToken? {
        return withContext(Dispatchers.IO) {
            val result = apiService.getRequestToken()
            return@withContext result.body()
        }
    }

    private suspend fun validateToken(user: String, password: String, token: RequestToken): RequestToken? {
        return withContext(Dispatchers.IO) {
            val result = apiService.postLoginRequest(user, password, token.requestToken)
            return@withContext result.body()
        }
    }

    private suspend fun getSession(token: RequestToken): SessionToken? {
        return withContext(Dispatchers.IO){
            val result = apiService.postSessionRequest(token.requestToken)
            return@withContext result.body()
        }
    }

    private suspend fun getUser(user:String, sessionId: String){
        withContext(Dispatchers.IO){
            val result = apiService.getUser(user, sessionId)
            if(result.isSuccessful && result.body() != null){
                databaseDao.insertUser(result.body()!!.toEntity(sessionId))
            }
        }
    }

    fun observeUser() = databaseDao.observeUser()

    //FAVORITES
    private suspend fun getAllFavorites(user: String, sessionId: String, page: Int = 1){
        withContext(Dispatchers.IO){

            val result = apiService.getFavorites(user, sessionId, page)
            if(result.isSuccessful && result.body() != null){

                result.body()!!.results.forEach { databaseDao.insertFavoritePage(FavoritePageEntity(it.id, page)) }
                databaseDao.insertTvShowList(result.body()!!.results)

                if(result.body()!!.totalPages>page){
                    getAllFavorites(user, sessionId, page+1)

                }else{}
            }else{
                Log.e("REPOSITORY", "Error on favorite api call code: ${result.code()}")
            }
        }
    }

    fun getFavAndShow(page: Int): LiveData<List<TvShowModel>> =
        Transformations.map(databaseDao.observeFavoriteAndShow(page)) {
            it.map { e -> e.toTvShowModel() }
        }

    suspend fun deleteUser(sessionId: String){
        withContext(Dispatchers.IO){
            Log.e("REPOSITORY", "SESSION = $sessionId TERMINATED")
            apiService.deleteSession(sessionId)
            databaseDao.deleteUser()
            databaseDao.deleteFavPage()
        }
    }
}