package example.android.data.network

import example.android.data.BuildConfig
import example.android.data.model.RequestToken
import example.android.data.model.SessionToken
import example.android.data.model.response.SeasonEpisodesResponse
import example.android.data.model.response.PageResultResponse
import example.android.data.model.response.TvShowResponse
import example.android.data.model.response.UserResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

const val API_KEY = BuildConfig.TMDB_API

interface ApiService {
    @GET("tv/top_rated?api_key=$API_KEY")
    fun getTopRatedTvShows(@Query("page") page: Int): Call<PageResultResponse>

    @GET("tv/airing_today?api_key=$API_KEY")
    fun getAiringTvShows(@Query("page") page: Int): Call<PageResultResponse>

    @GET("tv/popular?api_key=$API_KEY")
    fun getPopularTvShows(@Query("page") page: Int): Call<PageResultResponse>

    @GET("tv/on_the_air?api_key=$API_KEY")
    fun getOnAirTvShows(@Query("page") page: Int): Call<PageResultResponse>

    @GET("tv/{id}?api_key=$API_KEY")
    fun getTvShow(@Path("id") id : Int): Call<TvShowResponse>

    @GET("tv/{id}/{season}?api_key=$API_KEY")
    fun getSeason(
        @Path("id") id: Int,
        @Path("season") season: Int): Call<SeasonEpisodesResponse>

    @GET("authentication/token/new?api_key=$API_KEY")
    suspend fun getRequestToken(): Response<RequestToken>

    @FormUrlEncoded
    @POST("authentication/token/validate_with_login?api_key=$API_KEY")
    suspend fun postLoginRequest(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("request_token") requestToken: String
    ): Response<RequestToken>

    @FormUrlEncoded
    @POST("authentication/session/new?api_key=$API_KEY")
    suspend fun postSessionRequest(@Field("request_token") requestToken: String): Response<SessionToken>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "authentication/session?api_key=$API_KEY", hasBody = true)
    suspend fun deleteSession(@Field("session_id") sessionId: String)

    @GET("account/{account}?api_key=$API_KEY")
    suspend fun getUser(
        @Path("account") userId: String,
        @Query("session_id") sessionId: String
    ): Response<UserResponse>

    @GET("account/{account}/favorite/tv?api_key=${API_KEY}&language=en-US&sort_by=created_at.asc")
    suspend fun getFavorites(
        @Path("account") userId: String,
        @Query("session_id") sessionId: String,
        @Query("page") page: Int,
    ): Response<PageResultResponse>
}

