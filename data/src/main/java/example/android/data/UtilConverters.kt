package example.android.data

import example.android.data.model.entity.SeasonEntity
import example.android.data.model.entity.UserEntity
import example.android.data.model.models.SeasonModel
import example.android.data.model.models.TvShowModel
import example.android.data.model.models.UserModel
import example.android.data.model.response.TvSeasonResponse
import example.android.data.model.response.UserResponse

fun TvSeasonResponse.toEntity(id: Int): SeasonEntity {
    return SeasonEntity(
        id,
        this.id,
        this.overview,
        this.name,
        this.posterPath,
        this.seasonNumber,
        this.episodeCount
    )
}

fun SeasonEntity.toModel(): SeasonModel {
    return SeasonModel(
        this.showId,
        this.id,
        this.overview,
        this.name,
        this.posterPath,
        this.seasonNumber,
        this.episodeCount,
    )
}

fun PageAndShow.toTvShowModel(): TvShowModel{
    return if(this.tvShows != null){
        TvShowModel(
            this.tvShows!!.id,
            this.tvShows!!.name,
            this.tvShows!!.overview,
            this.tvShows!!.posterPath?:"",
            this.tvShows!!.voteAverage
        )
    }else{
        TvShowModel(0, "not found", "not found", "", 0f)
    }
}

fun UserResponse.toEntity(sessionId: String): UserEntity{
    return UserEntity(
        gravatar = this.avatar.gravatar.hash,
        avatar = this.avatar.tmdb.avatarPath,
        id = this.id,
        name = this.name,
        includeAdult = this.includeAdult,
        username = this.username,
        sessionId = sessionId
    )
}

fun UserEntity?.toModel(): UserModel?{
    if(this==null){
        return null
    }
    return UserModel(
        this.gravatar,
        this.avatar,
        this.name,
        this.includeAdult,
        this.username
    )
}

