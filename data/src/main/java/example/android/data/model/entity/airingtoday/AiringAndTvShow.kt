package example.android.data.model.entity.airingtoday

import androidx.room.Embedded
import androidx.room.Relation
import example.android.data.PageAndShow
import example.android.data.model.entity.TvShowEntity
import example.android.data.model.entity.toprated.TopRatedPageEntity

data class AiringAndTvShow(
@Embedded
override val page: TopRatedPageEntity,

@Relation(
    parentColumn = "showId",
    entityColumn = "id"
)
override val tvShows: TvShowEntity?
): PageAndShow