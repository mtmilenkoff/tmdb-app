package example.android.data.model.response

import com.squareup.moshi.Json
import example.android.data.model.entity.TvShowEntity

data class PageResultResponse(
    @Json(name = "page") val page: Int,
    @Json(name = "results") val results: List<TvShowEntity>,
    @Json(name = "total_pages") val totalPages: Int,
)