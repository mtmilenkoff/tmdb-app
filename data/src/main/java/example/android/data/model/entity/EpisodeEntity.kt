package example.android.data.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "episode")
data class EpisodeEntity(
    @Json(name = "episode_number") val episodeNumber: Int,
    @PrimaryKey(autoGenerate = false)
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String,
    @Json(name = "overview") val overview: String,
    @Json(name = "season_number") val seasonNumber: Int,
    @Json(name = "still_path") val stillPath: String
)