package example.android.data.model.entity.onair

import androidx.room.Entity
import androidx.room.PrimaryKey
import example.android.data.Page

@Entity(tableName = "on_air")
data class OnAirPageEntity(
    @PrimaryKey(autoGenerate = false)
    override val showId: Int,
    override val page: Int
): Page