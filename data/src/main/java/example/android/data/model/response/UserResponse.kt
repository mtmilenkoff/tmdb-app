package example.android.data.model.response

import com.squareup.moshi.Json

data class UserResponse (
    @Json(name = "avatar") val avatar: UserAvatarResponse,
    @Json(name = "id") val id: Int,
    @Json(name = "name") val name: String,
    @Json(name = "include_adult") val includeAdult: Boolean,
    @Json(name = "username") val username: String,
)