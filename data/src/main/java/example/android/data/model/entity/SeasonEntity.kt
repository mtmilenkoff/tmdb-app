package example.android.data.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "season")
data class SeasonEntity(
    val showId: Int,
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val overview: String,
    val name: String,
    val posterPath: String?,
    val seasonNumber: Int,
    val episodeCount: Int
)