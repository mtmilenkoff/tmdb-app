package example.android.data.model.entity.favorite

import androidx.room.Embedded
import androidx.room.Relation
import example.android.data.PageAndShow
import example.android.data.model.entity.TvShowEntity

data class FavoriteAndTvShows(
    @Embedded
    override val page: FavoritePageEntity,

    @Relation(
        parentColumn = "showId",
        entityColumn = "id"
    )
    override val tvShows: TvShowEntity?
): PageAndShow