package example.android.data.model.response

class UserAvatarResponse (
    val gravatar: UserGravatarResponse,
    val tmdb: UserTmdbResponse
)