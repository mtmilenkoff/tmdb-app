package example.android.data.model.entity.popular

import androidx.room.Entity
import androidx.room.PrimaryKey
import example.android.data.Page

@Entity(tableName = "popular")
data class PopularPageEntity(
    @PrimaryKey(autoGenerate = false)
    override val showId: Int,
    override val page: Int
): Page
