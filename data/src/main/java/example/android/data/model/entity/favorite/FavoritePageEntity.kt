package example.android.data.model.entity.favorite

import androidx.room.Entity
import androidx.room.PrimaryKey
import example.android.data.Page

@Entity(tableName = "favorite_page")
data class FavoritePageEntity(
    @PrimaryKey(autoGenerate = false)
    override val showId: Int,
    override val page: Int
): Page