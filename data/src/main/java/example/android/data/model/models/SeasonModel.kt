package example.android.data.model.models

data class SeasonModel (
    val tvShowId: Int,
    val id: Int,
    val overview: String,
    val name: String,
    val posterPath: String?,
    val seasonNumber: Int,
    val episodeCount: Int
    )