package example.android.data.model.entity.airingtoday

import androidx.room.Entity
import androidx.room.PrimaryKey
import example.android.data.Page

@Entity(tableName = "airing_page")
data class AiringPageEntity (
    @PrimaryKey(autoGenerate = false)
    override val showId: Int,
    override val page: Int
): Page