package example.android.data.model.response

import com.squareup.moshi.Json
import example.android.data.model.entity.EpisodeEntity

data class SeasonEpisodesResponse(
    @Json(name = "episodes") val episodes: List<EpisodeEntity>
)