package example.android.data.model

import com.squareup.moshi.Json

data class SessionToken(
    @Json(name = "success") val success: Boolean,
    @Json(name = "session_id") val sessionId: String
)
