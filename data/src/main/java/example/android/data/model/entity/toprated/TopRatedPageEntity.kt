package example.android.data.model.entity.toprated

import androidx.room.Entity
import androidx.room.PrimaryKey
import example.android.data.Page

@Entity(tableName = "top_rated")
data class TopRatedPageEntity(
    @PrimaryKey(autoGenerate = false)
    override val showId: Int,
    override val page: Int
): Page