package example.android.data.model.entity.toprated

import androidx.room.Embedded
import androidx.room.Relation
import example.android.data.PageAndShow
import example.android.data.model.entity.TvShowEntity

data class TopRatedAndTvShows(
    @Embedded
    override val page: TopRatedPageEntity,

    @Relation(
        parentColumn = "showId",
        entityColumn = "id"
    )
    override val tvShows: TvShowEntity?
): PageAndShow