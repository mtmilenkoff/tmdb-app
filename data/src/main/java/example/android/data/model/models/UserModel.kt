package example.android.data.model.models

data class UserModel (
    val gravatar: String,
    val avatar: String?,
    val name: String,
    val includeAdult: Boolean,
    val username: String,
)