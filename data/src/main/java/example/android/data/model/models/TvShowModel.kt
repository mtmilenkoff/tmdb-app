package example.android.data.model.models

data class TvShowModel(
    val id: Int,
    val name: String,
    val overview: String,
    val posterPath: String,
    val voteAverage: Float
)