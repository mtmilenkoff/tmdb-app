package example.android.data.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class UserEntity(
    val gravatar: String,
    val avatar: String?,
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val name: String,
    val includeAdult: Boolean,
    val username: String,
    val sessionId: String
)