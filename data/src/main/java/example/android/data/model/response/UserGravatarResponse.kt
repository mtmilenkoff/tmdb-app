package example.android.data.model.response

import com.squareup.moshi.Json

data class UserGravatarResponse(
    @Json(name = "hash") val hash: String
)
