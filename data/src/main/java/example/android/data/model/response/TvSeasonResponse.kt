package example.android.data.model.response

import com.squareup.moshi.Json

data class TvSeasonResponse(
    @Json(name = "episode_count") val episodeCount: Int,
    @Json(name = "id") val id: Int,
    @Json(name = "overview") val overview: String,
    @Json(name = "name") val name: String,
    @Json(name = "poster_path") val posterPath: String?,
    @Json(name = "season_number") val seasonNumber: Int,
)