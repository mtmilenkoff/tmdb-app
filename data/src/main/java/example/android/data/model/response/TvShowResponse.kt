package example.android.data.model.response

import com.squareup.moshi.Json

data class TvShowResponse (
    @Json(name = "seasons") val seasons: List<TvSeasonResponse>
)