package example.android.data.model

import com.squareup.moshi.Json

data class RequestToken(
    @Json(name = "success") val success: Boolean,
    @Json(name = "request_token") val requestToken: String
)
