package example.android.data.model.response

import com.squareup.moshi.Json

data class UserTmdbResponse (
    @Json(name = "avatar_path") val avatarPath: String
    )