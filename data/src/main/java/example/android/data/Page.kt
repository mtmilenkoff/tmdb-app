package example.android.data

interface Page {
    val showId: Int
    val page: Int
}