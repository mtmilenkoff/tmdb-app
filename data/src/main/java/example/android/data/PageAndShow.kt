package example.android.data

import example.android.data.model.entity.TvShowEntity


interface PageAndShow {
    val page: Page
    val tvShows: TvShowEntity?
}