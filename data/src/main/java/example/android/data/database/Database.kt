package example.android.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import example.android.data.model.entity.EpisodeEntity
import example.android.data.model.entity.SeasonEntity
import example.android.data.model.entity.toprated.TopRatedPageEntity
import example.android.data.model.entity.TvShowEntity
import example.android.data.model.entity.UserEntity
import example.android.data.model.entity.airingtoday.AiringPageEntity
import example.android.data.model.entity.favorite.FavoritePageEntity
import example.android.data.model.entity.onair.OnAirPageEntity
import example.android.data.model.entity.popular.PopularPageEntity


@Database(entities = [
    TopRatedPageEntity::class,
    AiringPageEntity::class,
    OnAirPageEntity::class,
    PopularPageEntity::class,
    FavoritePageEntity::class,
    TvShowEntity::class,
    SeasonEntity::class,
    EpisodeEntity::class,
    UserEntity::class ], version = 1, exportSchema = false)
abstract class Database : RoomDatabase() {
    abstract val dao: DatabaseDao
}