package example.android.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import example.android.data.model.entity.*
import example.android.data.model.entity.airingtoday.AiringAndTvShow
import example.android.data.model.entity.airingtoday.AiringPageEntity
import example.android.data.model.entity.favorite.FavoriteAndTvShows
import example.android.data.model.entity.favorite.FavoritePageEntity
import example.android.data.model.entity.onair.OnAirAndTvShow
import example.android.data.model.entity.onair.OnAirPageEntity
import example.android.data.model.entity.popular.PopularAndTvShow
import example.android.data.model.entity.popular.PopularPageEntity
import example.android.data.model.entity.toprated.TopRatedAndTvShows
import example.android.data.model.entity.toprated.TopRatedPageEntity

@Dao
interface DatabaseDao {

    //Top Rated Page
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTopRated(topRatedPageEntity: TopRatedPageEntity)
    @Transaction @Query("SELECT * FROM top_rated WHERE page = :page")
    fun observeTopAndShow(page: Int) : LiveData<List<TopRatedAndTvShows>>

    //Airing Today
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAiring(airingPageEntity: AiringPageEntity)
    @Transaction @Query("SELECT * FROM airing_page WHERE page = :page")
    fun observeAiringAndShow(page: Int) : LiveData<List<AiringAndTvShow>>

    //On Air
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOnAir(onAirPageEntity: OnAirPageEntity)
    @Transaction @Query("SELECT * FROM on_air WHERE page = :page")
    fun observeOnAirAndShow(page: Int) : LiveData<List<OnAirAndTvShow>>

    //Popular
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPopular(popularPageEntity: PopularPageEntity)
    @Transaction @Query("SELECT * FROM popular WHERE page = :page")
    fun observePopularAndShow(page: Int) : LiveData<List<PopularAndTvShow>>

    //Tv Shows
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertTvShowList(tvShows: List<TvShowEntity>)

    //Seasons
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSeasonPage(season: List<SeasonEntity>)
    @Query("SELECT * FROM season WHERE showId = :id")
    fun observeSeasons(id: Int) : LiveData<List<SeasonEntity>>

    //Episodes
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEpisodeList(episode: List<EpisodeEntity>)
    @Query("SELECT * FROM episode WHERE seasonNumber = :season")
    fun observeEpisodes(season: Int) : LiveData<List<EpisodeEntity>>

    //User
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: UserEntity)
    @Query("SELECT * FROM user")
    fun observeUser() : LiveData<UserEntity>

    //Favorites
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavoritePage(favoritePageEntity: FavoritePageEntity)
    @Transaction
    @Query("SELECT * FROM favorite_page WHERE page = :page")
    fun observeFavoriteAndShow(page: Int) : LiveData<List<FavoriteAndTvShows>>

    @Query("DELETE FROM user")
    suspend fun deleteUser()
    @Query("DELETE FROM favorite_page")
    suspend fun deleteFavPage()
}