package example.android.data.database

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(app: Application): Database{
        return Room.databaseBuilder(
            app.applicationContext,
            Database::class.java,
            "main_database"
        ).build()
    }

    @Singleton
    @Provides
    fun provideDao(database: Database) = database.dao
}