package example.android.tmdbapp.seasons.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import example.android.data.model.models.SeasonModel
import example.android.tmdbapp.databinding.CardSeasonListBinding

class SeasonListAdapter : ListAdapter<SeasonModel, SeasonHolder>(SeasonDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonHolder {
        return SeasonHolder(
            CardSeasonListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SeasonHolder, position: Int) {
        val show = getItem(position)
        holder.bind(show)
    }

}