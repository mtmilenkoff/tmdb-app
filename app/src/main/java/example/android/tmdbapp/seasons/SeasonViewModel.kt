package example.android.tmdbapp.seasons

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import example.android.domain.usecases.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SeasonViewModel @Inject constructor(
    private val updateSeason: UseCaseUpdateSeason,
    private val observeSeason: UseCaseObserveSeason
) : ViewModel(){

    private val showId = MutableLiveData<Int>()
    val seasonsList = Transformations.switchMap(showId) { observeSeason(it) }

    fun updateSeason(id: Int){
        viewModelScope.launch {
            updateSeason.invoke(id)
            showId.postValue(id)
        }
    }
}