package example.android.tmdbapp.seasons.recyclerview

import androidx.recyclerview.widget.DiffUtil
import example.android.data.model.models.SeasonModel

class SeasonDiffCallback : DiffUtil.ItemCallback<SeasonModel>() {
    override fun areItemsTheSame(oldItem: SeasonModel, newItem: SeasonModel) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: SeasonModel, newItem: SeasonModel) = oldItem == newItem
}