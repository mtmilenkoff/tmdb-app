package example.android.tmdbapp.seasons.recyclerview

import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import example.android.data.model.models.SeasonModel
import example.android.tmdbapp.R
import example.android.tmdbapp.databinding.CardSeasonListBinding
import example.android.tmdbapp.seasons.SeasonActivity

class SeasonHolder (binding: CardSeasonListBinding) : RecyclerView.ViewHolder(binding.root)  {

    private val seasonThumbnail = binding.seasonThumbnail
    private val seasonNumber = binding.seasonName
    private val seasonEpisodes = binding.seasonEpisodes
    private val seasonSummary = binding.seasonSummary
    private val seasonLayout = binding.seasonLayout

    fun bind(season: SeasonModel){
        seasonNumber.text = season.name
        seasonEpisodes.text = "Episodes ${season.episodeCount}"
        seasonSummary.text =  season.overview
        val thumbnailURL = "https://image.tmdb.org/t/p/w200" + season.posterPath

        Glide.with(seasonThumbnail.context)
            .load(thumbnailURL)
            .placeholder(R.drawable.downloading_image)
            .error(R.drawable.broken_image)
            .into(seasonThumbnail)

        seasonLayout.setOnClickListener {
            //CHANGE INTENT ACTIVITY TO EPISODE ACTIVITY
            val intentDetails = Intent(seasonLayout.context, SeasonActivity::class.java).also {
                //INSERT EXTRAS FOR EPISODE LIST
            }
            ContextCompat.startActivity(seasonLayout.context, intentDetails, null)
        }
    }
}