package example.android.tmdbapp.seasons

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import example.android.tmdbapp.R
import example.android.tmdbapp.databinding.ActivitySeasonBinding
import example.android.tmdbapp.seasons.recyclerview.SeasonListAdapter
import example.android.tmdbapp.categories.*

@AndroidEntryPoint
class SeasonActivity : AppCompatActivity(){

    private lateinit var binding: ActivitySeasonBinding
    private val viewModel: SeasonViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySeasonBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()

        //Set show overview
        binding.seasonOverview.text = intent.getStringExtra("overview")

        //Set show poster
        val posterUrl = "https://image.tmdb.org/t/p/w500" + intent.getStringExtra("posterPath")
        Glide.with(this)
            .load(posterUrl)
            .placeholder(R.drawable.downloading_image)
            .error(R.drawable.broken_image)
            .into(binding.seasonPoster)

        //Update db with show seasons
        val showId = intent.getIntExtra("id", 0)
        viewModel.updateSeason(showId)

        //Update recyclerview with seasons from db
        viewModel.seasonsList.observe(this) {
            (binding.seasonList.adapter as SeasonListAdapter).submitList(it)
        }
    }

    //Recyclerview setup
    private fun setupRecyclerView() = binding.seasonList.apply {
        adapter = SeasonListAdapter()
        layoutManager = LinearLayoutManager(this@SeasonActivity, RecyclerView.VERTICAL, false)
    }
}