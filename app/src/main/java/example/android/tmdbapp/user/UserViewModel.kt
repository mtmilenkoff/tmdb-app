package example.android.tmdbapp.user

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import example.android.domain.usecases.UseCaseDeleteUser
import example.android.domain.usecases.UseCaseFavAndShow
import example.android.domain.usecases.UseCaseObserveUser
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
    private val getFavorites: UseCaseFavAndShow,
    observeUser: UseCaseObserveUser,
    private val deleteUser: UseCaseDeleteUser
) : ViewModel(){

    private val page = MutableLiveData<Int>()
    val favoriteList = Transformations.switchMap(page) { getFavorites(it) }
    val user = observeUser()
    val loggedOff = MutableLiveData(false)

    init {
        getResults(2)
    }

    fun getResults(toPage: Int) {
        viewModelScope.launch {
            page.postValue(toPage)
        }
    }

    fun logOff(){
        viewModelScope.launch {
            user.value?.let { deleteUser(it.sessionId) }
            loggedOff.postValue(true)
        }
    }

}