package example.android.tmdbapp.user

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import example.android.tmdbapp.R
import example.android.tmdbapp.databinding.ActivityUserBinding
import example.android.tmdbapp.login.LoginActivity
import example.android.tmdbapp.recyclerview.TvShowListAdapter

@AndroidEntryPoint
class UserActivity: AppCompatActivity() {

    private lateinit var binding: ActivityUserBinding
    private val viewModel: UserViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()

        viewModel.user.observe(this, {
            if(it != null){
                val avatar = it.avatar
                if(avatar != null){
                    setImage(binding.userPicture, "https://image.tmdb.org/t/p/w200/$avatar")
                }else{
                    setImage(binding.userPicture, "https://www.gravatar.com/avatar/${it.gravatar}")
                }
                binding.username.text = it.username
            }
        })

        viewModel.favoriteList.observe(this, {
            (binding.favoriteList.adapter as TvShowListAdapter).submitList(it)
        })

        binding.UserButton.setOnClickListener {
            viewModel.logOff()
        }

        viewModel.loggedOff.observe(this, {
            if(it == true){
                val intent = Intent(this, LoginActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)

                }
                startActivity(intent)
            }
        })
    }

    private fun setupRecyclerView() = binding.favoriteList.apply {
        adapter = TvShowListAdapter()
        layoutManager = LinearLayoutManager(this@UserActivity,  RecyclerView.HORIZONTAL, false)
    }

    private fun setImage(view: ImageView, url: String){
        Glide.with(view.context)
            .load(url)
            .placeholder(R.drawable.downloading_image)
            .error(R.drawable.broken_image)
            .into(view)
    }
}