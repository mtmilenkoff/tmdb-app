package example.android.tmdbapp.recyclerview

import androidx.recyclerview.widget.DiffUtil
import example.android.data.model.models.TvShowModel

class TvShowDiffCallback : DiffUtil.ItemCallback<TvShowModel>() {
    override fun areItemsTheSame(oldItem: TvShowModel, newItem: TvShowModel) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: TvShowModel, newItem: TvShowModel) = oldItem == newItem
}
