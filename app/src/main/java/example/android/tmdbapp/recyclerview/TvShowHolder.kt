package example.android.tmdbapp.recyclerview

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import example.android.data.model.models.TvShowModel
import example.android.tmdbapp.R
import example.android.tmdbapp.databinding.CardTvshowListBinding
import example.android.tmdbapp.seasons.SeasonActivity

class TvShowHolder (binding: CardTvshowListBinding) : RecyclerView.ViewHolder(binding.root) {

    private val cardThumbnail: ImageView = binding.thumbnail
    private val cardRating: TextView = binding.cardRating
    private val cardTitle: TextView = binding.cardTitle
    private val cardLayout: ConstraintLayout = binding.cardLayout

    fun bind(show: TvShowModel){
        cardTitle.text = show.name
        cardRating.text = show.voteAverage.toString()
        val thumbnailURL = "https://image.tmdb.org/t/p/w200" + show.posterPath

        Glide.with(cardThumbnail.context)
            .load(thumbnailURL)
            .placeholder(R.drawable.downloading_image)
            .error(R.drawable.broken_image)
            .into(cardThumbnail)

        cardLayout.setOnClickListener {
            val intentSeason = Intent(cardLayout.context, SeasonActivity::class.java).also {
                it.putExtra("id", show.id)
                it.putExtra("name", show.name)
                it.putExtra("overview", show.overview)
                it.putExtra("posterPath", show.posterPath)
            }
            ContextCompat.startActivity(cardLayout.context, intentSeason, null)
        }
    }
}