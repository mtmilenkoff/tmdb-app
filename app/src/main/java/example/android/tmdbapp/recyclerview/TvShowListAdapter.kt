package example.android.tmdbapp.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import example.android.data.model.models.TvShowModel
import example.android.tmdbapp.databinding.CardTvshowListBinding

class TvShowListAdapter : ListAdapter<TvShowModel, TvShowHolder>(TvShowDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowHolder {
        return TvShowHolder(
            CardTvshowListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }
    override fun onBindViewHolder(holder: TvShowHolder, position: Int) {
        val show = getItem(position)
        holder.bind(show)
    }
}
