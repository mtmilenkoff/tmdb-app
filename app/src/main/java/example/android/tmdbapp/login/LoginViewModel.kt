package example.android.tmdbapp.login

import android.util.Log
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import example.android.domain.usecases.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val postLogin: UseCasePostLogin,
    observeUser: UseCaseObserveUser) : ViewModel(){

    enum class LogState {
        SUCCESS, ERROR, PENDING
    }

    val logState = MutableLiveData(LogState.PENDING)
    val user = observeUser()

    fun getSession(username: String, password: String){
        viewModelScope.launch {
            val loginSuccess = postLogin(username, password)
            if(loginSuccess){
                logState.postValue(LogState.SUCCESS)
            }else{
                Log.e("REPOSITORY", "Error on login")
                logState.postValue(LogState.ERROR)
            }
        }
    }

    fun resetLogstate(){
        logState.postValue(LogState.PENDING)
    }
}