package example.android.tmdbapp.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import example.android.tmdbapp.R
import example.android.tmdbapp.databinding.ActivityLoginBinding
import example.android.tmdbapp.categories.CategoriesActivity


@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private val viewModel : LoginViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_TMDBApp)
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val username = binding.usernameField
        val password = binding.passwordField
        val intent = Intent(this, CategoriesActivity::class.java)

        binding.button.setOnClickListener {
            viewModel.getSession(username.text.toString(), password.text.toString())
        }

        viewModel.user.observe(this, {
            if(it != null){
                startActivity(intent)
            }
        })

        viewModel.logState.observe(this,{
            when(it){
                LoginViewModel.LogState.PENDING -> println("waiting")
                LoginViewModel.LogState.SUCCESS -> startActivity(intent)
                LoginViewModel.LogState.ERROR -> {
                    Toast.makeText(this, "Error on login\nCheck username and password", Toast.LENGTH_SHORT).show()
                    viewModel.resetLogstate()
                }
                else -> Log.e("LOGIN ACTIVITY", "Error on log")
            }
        })
    }
}