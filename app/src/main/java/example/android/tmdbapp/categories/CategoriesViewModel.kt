package example.android.tmdbapp.categories

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import example.android.domain.usecases.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoriesViewModel @Inject constructor(
    getTopRated: UseCaseObserveTopAndShow,
    private val updateTopRated: UseCaseUpdateTopRated,
    getAiring: UseCaseObserveAiringShows,
    private val updateAiring: UseCaseUpdateAiringShows,
    getPopular: UseCaseObservePopular,
    private val updatePopular: UseCaseUpdatePopular,
    getOnAir: UseCaseObserveOnAir,
    private val updateOnAir: UseCaseUpdateOnAir
) : ViewModel(){

    enum class Categories{
        TOP_RATED, POPULAR, ON_AIR, AIRING_TODAY
    }

    private val page = MutableLiveData(1)
    val popularList = Transformations.switchMap(page) { getPopular(it) }
    val onAirList = Transformations.switchMap(page) { getOnAir(it) }
    val topRatedList = Transformations.switchMap(page) { getTopRated(it) }
    val airingTodayList = Transformations.switchMap(page) { getAiring(it) }

    init {
        updateTopRatedPage(1)
        updateAiringPage(1)
        updatePopularPage(1)
        updateOnAirPage(1)
    }

    fun updateTopRatedPage(page: Int) {
        viewModelScope.launch {
            updateTopRated(page)
        }
    }

    fun updateAiringPage(page: Int) {
        viewModelScope.launch {
            updateAiring(page)
        }
    }

    fun updatePopularPage(page: Int) {
        viewModelScope.launch {
            updatePopular(page)
        }
    }

    fun updateOnAirPage(page: Int) {
        viewModelScope.launch {
            updateOnAir(page)
        }
    }

    fun nextPage(cat: Categories){
        when(cat){
            Categories.TOP_RATED -> updateTopRatedPage(page.value!!+1)
            Categories.POPULAR -> updatePopularPage(page.value!!+1)
            Categories.ON_AIR -> updateOnAirPage(page.value!!+1)
            Categories.AIRING_TODAY -> updateAiringPage(page.value!!+1)
        }
        page.postValue(page.value!!+1)
    }

    fun prevPage(cat: Categories){
        if(page.value!!>1){
            when(cat){
                Categories.TOP_RATED -> updateTopRatedPage(page.value!!-1)
                Categories.POPULAR -> updatePopularPage(page.value!!-1)
                Categories.ON_AIR -> updateOnAirPage(page.value!!-1)
                Categories.AIRING_TODAY -> updateAiringPage(page.value!!-1)
            }
            page.postValue(page.value!!-1)
        }
    }

    fun resetPage(){
        page.postValue(1)
    }



}
