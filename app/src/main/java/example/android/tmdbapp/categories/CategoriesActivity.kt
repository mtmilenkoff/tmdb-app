package example.android.tmdbapp.categories

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import example.android.tmdbapp.R
import example.android.tmdbapp.databinding.ActivityCategoriesBinding
import example.android.tmdbapp.recyclerview.TvShowListAdapter
import example.android.tmdbapp.user.UserActivity

@AndroidEntryPoint
class CategoriesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCategoriesBinding
    private val viewModel: CategoriesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCategoriesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Setup recyclerview
        val adapter = TvShowListAdapter()
        binding.showList.adapter = adapter
        binding.showList.layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)

        //Start by observing Top Rated
        var category = CategoriesViewModel.Categories.TOP_RATED
        viewModel.topRatedList.observe(this, { adapter.submitList(it) })
        binding.buttonTopRated.setBackgroundColor(Color.BLUE)

        binding.buttonTopRated.setOnClickListener { button ->
            resetCategories()
            category = CategoriesViewModel.Categories.TOP_RATED
            viewModel.topRatedList.observe(this, { adapter.submitList(it) })
            button.setBackgroundColor(Color.BLUE)
        }

        binding.buttonAiringToday.setOnClickListener { button ->
            resetCategories()
            category = CategoriesViewModel.Categories.AIRING_TODAY
            viewModel.airingTodayList.observe(this, { adapter.submitList(it) })
            button.setBackgroundColor(Color.BLUE)
        }

        binding.buttonOnAir.setOnClickListener { button ->
            resetCategories()
            category = CategoriesViewModel.Categories.ON_AIR
            viewModel.onAirList.observe(this, { adapter.submitList(it) })
            button.setBackgroundColor(Color.BLUE)
        }

        binding.buttonPopular.setOnClickListener { button ->
            resetCategories()
            category = CategoriesViewModel.Categories.POPULAR
            viewModel.popularList.observe(this, { adapter.submitList(it) })
            button.setBackgroundColor(Color.BLUE)
        }

        binding.buttonNext.setOnClickListener { viewModel.nextPage(category) }
        binding.buttonPrevious.setOnClickListener { viewModel.prevPage(category) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_user_profile, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when(item.itemId){
            R.id.actionProfile -> {
                startActivity(Intent(this, UserActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onBackPressed() {
        super.onBackPressed()
        moveTaskToBack(true)
    }

    private fun resetCategories(){
        binding.buttonAiringToday.setBackgroundColor(Color.GRAY)
        viewModel.airingTodayList.removeObservers(this)

        binding.buttonOnAir.setBackgroundColor(Color.GRAY)
        viewModel.onAirList.removeObservers(this)

        binding.buttonPopular.setBackgroundColor(Color.GRAY)
        viewModel.popularList.removeObservers(this)

        binding.buttonTopRated.setBackgroundColor(Color.GRAY)
        viewModel.topRatedList.removeObservers(this)

        viewModel.resetPage()
    }
}
